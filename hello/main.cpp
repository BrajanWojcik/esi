#include <gtk/gtk.h>

/* Definicja pliku z zawartoœci¹ GUI */
#define UI_FILE "hello.ui"


GtkWidget * utworz_okno( void )
{
    GtkWidget * okno;
    GtkBuilder * builder;
    GError * error = NULL;

    /* Tworzy obiekt buildera */
    builder = gtk_builder_new();
    /* Wczytuje zawartoœæ interfejsu i sprawdza ewentualne b³êdy */
    if( !gtk_builder_add_from_file( builder, UI_FILE, & error ) )
    {
        g_warning( "Nie mo¿na wczytaæ plilu buildera: %s", error->message );
        g_error_free( error );
    }

    /* £¹czy sygna³y zawarte w pliku interfejsu z odpowiednimi funkcjami */
    gtk_builder_connect_signals( builder, NULL );

    /* Pobiera obiekt z nazw¹ "window1" */
    okno = GTK_WIDGET( gtk_builder_get_object( builder, "window1" ) );

    /* Obiekt buildera nie bêdzie ju¿ nam potrzebny, wiêc go "zwalniamy" */
    g_object_unref( builder );

    return okno;
}


int
main( int argc, char * argv[] )
{
    GtkWidget * okno;


//    gtk_set_locale();
    gtk_init( & argc, & argv );

    okno = utworz_okno();
    gtk_widget_show( okno );

    gtk_main();
    return 0;
}
